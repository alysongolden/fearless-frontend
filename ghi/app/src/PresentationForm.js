import React, { useState, useEffect } from "react";

function PresentationForm() {
  const [conferences, setConferences] = useState([]);
  const [formData, setFormData] = useState({
    presenter_name: "",
    presenter_email: "",
    company_name: "",
    title: "",
    synopsis: "",
    conference: ""
  });

  useEffect(() => {
    async function fetchConferences() {
      try {
        const response = await fetch("http://localhost:8000/api/conferences/");
        if (response.ok) {
          const data = await response.json();
          setConferences(data.conferences);
        } else {
          throw new Error("Failed to fetch conferences");
        }
      } catch (error) {
        console.error("Error:", error);
      }
    }

    fetchConferences();
  }, []);

  const handleInputChange = e => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value
    });
  };

  const handleSubmit = async e => {
    e.preventDefault();
    const conferenceId = formData.conference;

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json"
      }
    };

    try {
      const response = await fetch(
        `http://localhost:8000/api/conferences/${conferenceId}/presentations/`,
        fetchConfig
      );
      if (response.ok) {
        setFormData({
          presenter_name: "",
          presenter_email: "",
          company_name: "",
          title: "",
          synopsis: "",
          conference: ""
        });
        alert("Presentation submitted successfully!");
      } else {
        throw new Error("Failed to create presentation");
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  type="text"
                  name="presenter_name"
                  className="form-control"
                  placeholder="Presenter name"
                  value={formData.presenter_name}
                  onChange={handleInputChange}
                  required
                />
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  type="email"
                  name="presenter_email"
                  className="form-control"
                  placeholder="Presenter email"
                  value={formData.presenter_email}
                  onChange={handleInputChange}
                  required
                />
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  type="text"
                  name="company_name"
                  className="form-control"
                  placeholder="Company name"
                  value={formData.company_name}
                  onChange={handleInputChange}
                />
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  type="text"
                  name="title"
                  className="form-control"
                  placeholder="Title"
                  value={formData.title}
                  onChange={handleInputChange}
                  required
                />
                <label htmlFor="title">Title</label>
              </div>
              <div className="form-floating mb-3">
                <textarea
                  className="form-control"
                  name="synopsis"
                  id="synopsis"
                  rows="3"
                  placeholder="Synopsis"
                  value={formData.synopsis}
                  onChange={handleInputChange}
                ></textarea>
                <label htmlFor="synopsis">Synopsis</label>
              </div>
              <div className="mb-3">
                <select
                  name="conference"
                  id="conference"
                  className="form-select"
                  value={formData.conference}
                  onChange={handleInputChange}>
                    <option value="">Choose a conference</option>
                    {conferences.map(conference => {
                        return (
                            <option key={conference.id} value={conference.id}>
                            {conference.name}
                            </option>)
                        }
                        )
                        }
                </select>
              </div>
              <button type="submit" className="btn btn-primary">
                Create
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PresentationForm;
