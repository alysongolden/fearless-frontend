import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Nav from "./Nav";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendConferenceForm from "./AttendConferenceForm";
import AttendeesList from "./AttendeesList";
import MainPage from "./MainPage";
import PresentationForm from "./PresentationForm";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return <Router>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="/locations/new" element={<LocationForm />} />
          <Route path="/conferences/new" element={<ConferenceForm />} />
          <Route path="/attendees/new" element={<AttendConferenceForm />} />
          <Route path="/presentations/new" element={<PresentationForm />} />
          <Route path="/attendees" element={<AttendeesList attendees={props.attendees} />} />
        </Routes>
      </div>
    </Router>;
}

export default App;
